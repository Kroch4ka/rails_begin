class ApplicationController < ActionController::Base
  def text
    render plain: "#{"\u00BF".encode('utf-8')}Hola, mundo!"
  end
end
